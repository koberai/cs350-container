#! /bin/bash
export LOGS=/logs
export OS161_DIR="/os161-1.99"
export PATH=$PATH:/os161/tools/bin

unpack_kernel() {
	mkdir /kernel 
	tar -xf /assignment.tgz -C /kernel --strip-components 1
}

_clean_kernel_h() {
    set -e
    cd /os161-1.99/kern/compile/$1
    bmake clean
    cd -
}

clean_kernel() {
    _clean_kernel_h $1 &
    wait
}

build_k_helper() {
	cd $OS161_DIR
	mkdir /os-compile 2> /dev/null
	cp -r /kernel/kern/* $OS161_DIR/kern/
	rm -rf $OS161_DIR/kern/compile/* 2> /dev/null
	set -e
	# Configure - step 1
	echo "[CS350] $1 Configuring Kernel..."
	touch $LOGS/configure.log
	truncate -s 0 $LOGS/configure.log
	./configure --ostree=/os-compile --toolprefix=mips-harvard-os161- >> $LOGS/configure.log
	cd kern/conf
	./config $1 >> $LOGS/configure.log

	# Depends - step 2
	cd ../compile/$1
	echo "[CS350] $1 Making Kernel..."
	touch $LOGS/depend.log
	truncate -s 0 $LOGS/depend.log
	bmake depend >> $LOGS/depend.log 2>> $LOGS/depend.log

	# Make and install - step 3
	touch $LOGS/make.log
	truncate -s 0 $LOGS/make.log
	bmake WERROR= >> $LOGS/make.log 2>> $LOGS/make.log
	bmake install >> $LOGS/make.log 2>> $LOGS/make.log

	cd $OS161_DIR
	echo "Building Userspace programs" >> $LOGS/make.log
	bmake WERROR= >> $LOGS/make.log 2>> $LOGS/make.log
	bmake install >> $LOGS/make.log 2>> $LOGS/make.log

	cd /os-compile/
	cp /sys161.conf .
}

build_kernel() {
	if [ "$#" -ne 1 ]; then
		echo "Usage: build_kernel <ASST0, ASST1 ...>"
	fi
	
	build_k_helper $1 &
	wait
}

test_kernel() {
	if [ "$#" -ne 1 ]; then
		echo "Usage: test_kernel <ASST0, ASST1 ...>"
	fi

	cd /os-compile
	echo "[CS350] Tests $1"
	echo "============="
	# Run public tests - step 4
	for f in /assignments/$1/public
	do
		LOG=$LOGS/test_`basename $f`.log
		TOTAL=$(wc -l < $f)
		DONE=0
		touch $LOG
		truncate -s 0 $LOG
		for p in `cat $f` 
		do
			echo ">SPLIT<" >> $LOG
			echo ">TEST=$p<" >> $LOG
			sys161 kernel "$p;q" >> $LOG 2> /dev/null
			DONE=$((DONE+1))
			echo "[CS350] Running Tests [$DONE/$TOTAL]"
		done
	done
	cd - > /dev/null
}


